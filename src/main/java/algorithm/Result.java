package algorithm;

public class Result {
    public Person younger;
    public Person older;
    public long distance;

    public Person getYounger() {
        return younger;
    }

    public void setYounger(Person younger) {
        this.younger = younger;
    }

    public Person getOlder() {
        return older;
    }

    public void setOlder(Person older) {
        this.older = older;
    }

    public long getDistance() {
        return distance;
    }

    public void setDistance(long distance) {
        this.distance = distance;
    }
}