package algorithm;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Finder {
    private final List<Person> people;
    private final Comparator<Result> SHORTEST_DISTANCE = (result1, result2) -> (int) (result1.getDistance() - result2.getDistance() < 0 ? -1
            : (result1.getDistance() - result2.getDistance() > 0 ? 1 : 0));
    private final Comparator<Result> LARGEST_DISTANCE = (r1, r2) -> (int) (r1.getDistance() - r2.getDistance() < 0 ? 1
            : (r1.getDistance() - r2.getDistance() > 0 ? -1 : 0));

    public Finder(List<Person> people) {
        this.people = people;
    }

    public Result findShortestDistance() {
        return find(SHORTEST_DISTANCE);
    }

    public Result findLargestDistance() {
        return find(LARGEST_DISTANCE);
    }

    public Result find(Comparator<Result> comparator) {
        List<Result> candidates = getCandidates();

        if (candidates.size() < 1) {
            return new Result();
        }

        Result answer = candidates.get(0);
        for (Result candidate : candidates) {
            if (comparator.compare(candidate, answer) < 0) {
                answer = candidate;
            }
        }
        return answer;
    }

    private List<Result> getCandidates() {
        List<Result> tr = new ArrayList<Result>();

        for (int i = 0; i < people.size() - 1; i++) {
            for (int j = i + 1; j < people.size(); j++) {
                Result result = new Result();
                if (people.get(i).getBirthDate().getTime() < people.get(j).getBirthDate().getTime()) {
                    result.setYounger(people.get(i));
                    result.setOlder(people.get(j));
                } else {
                    result.setYounger(people.get(j));
                    result.setOlder(people.get(i));
                }
                result.setDistance(
                        result.getOlder().getBirthDate().getTime() - result.getYounger().getBirthDate().getTime());
                tr.add(result);
            }
        }
        return tr;
    }
}