package algorithm;

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class Person implements Comparable<Person> {
    public String name;
    public Date birthDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Person)) {
        }
        Person castOther = (Person) other;
        return new EqualsBuilder().append(name, castOther.name).append(birthDate, castOther.birthDate).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(name).append(birthDate).toHashCode();
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @Override
    public int compareTo(Person other) {
        if (this == other) {
            return 0;
        } else {
            return getBirthDate().compareTo(other.getBirthDate());
        }
    }
}